using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

namespace Assets.ScriptableObject
{
    [CreateAssetMenu(menuName = "SO/SOscene")]
    public class SOScenes : UnityEngine.ScriptableObject
    {
        [Serializable]
        public enum SceneIdNames
        {
            MainMenu,
            Lobby,
            Arena
        }

        [Serializable]
        public struct Scene
        {
            public SceneIdNames idName;
            public string buildSceneName;

        }

        [SerializeField]
        public List<Scene> sceneList;
    }
}
