﻿using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI
{
    public class PlayerLobbyCard : MonoBehaviour
    {
        public TMP_Text playerPseudoTMP;

        public uint PlayerID { get; set; }

        public string PlayerPseudo 
        { 
            get => playerPseudoTMP.text; 
            set => playerPseudoTMP.text = value; 
        }

        public Image SpriteReady;

        public GameObject BackgrounPanel;

        private bool _isReady;
        public bool IsReady 
        { 
            get => _isReady;
            set
            {
                _isReady = value;

                SpriteReady.color = value ? Color.green : Color.white;
            } 
        }

        public bool PlayerConnected { get; set; }

        public int IndexTeam { get; set; }

        public void Connect(string playerPseudo, uint playerID)
        {
            SetObjectVisibility(true);
            PlayerPseudo = playerPseudo;
            PlayerConnected = true;
            PlayerID = playerID;
        }

        public void Leave()
        {
            PlayerPseudo = "";
            PlayerConnected = false;
            PlayerID = 0;
            SetObjectVisibility(false);
        }

        private void SetObjectVisibility(bool isVisible)
        {
            playerPseudoTMP.gameObject.SetActive(isVisible);
            SpriteReady.gameObject.SetActive(isVisible);
            BackgrounPanel.SetActive(isVisible);
        }
    }
}
