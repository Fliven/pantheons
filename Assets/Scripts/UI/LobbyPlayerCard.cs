﻿using System.Collections;
using UnityEngine;
using TMPro;

namespace Assets.Scripts.UI
{
    public class LobbyPlayerCard : MonoBehaviour
    {
        [Header("References")]
        public TMP_Text m_PlayerCardName;

        public bool IsOwnByPlayer { get; private set; }
        public bool IsReady { get; set; }

        public uint ID { get; set; }


        public void Init(string playerName)
        {
            m_PlayerCardName.text = playerName;
            IsOwnByPlayer = true;
        }
    }
}