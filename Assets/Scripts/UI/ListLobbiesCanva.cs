﻿using Assets.Scripts.Manager;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using TMPro;
using UnityEngine;

namespace Assets.Scripts.UI
{
    public class ListLobbiesCanva : MonoBehaviour
    {

        [Header("References")]
        public TMP_Text m_NewLobbyName;
        public GameObject m_ContentScrollView;
        public MainMenuCanvas m_MainMenuPanel;
        public LobbyCanva m_LobbyPanel;
        public GameObject m_LobbyItemsToInstantiate;


        private List<LobbyListItem> _listLobbies;
        public string PlayerName { get; set; }
        private string _selectedLobby;

        private void Awake()
        {
            _listLobbies = new();
        }

        private void OnEnable()
        {
            MyNetworkManager.Instance.InitClient(m_MainMenuPanel.PlayerPseudo);
            MyNetworkManager.Instance.OnFoundLobbies += CreateLobbyItemInList;
            GetNETLobbies();
        }

        private void OnDisable()
        {
            MyNetworkManager.Instance.OnFoundLobbies -= CreateLobbyItemInList;
            EmptyListLobbies();
        }

        public void ReturnToMenu()
        {
            MyNetworkManager.Instance.ReturnMainMenu();
            m_MainMenuPanel.gameObject.SetActive(true);
            gameObject.SetActive(false);
        }

        public void CreateLobby()
        {
            string newLobbyname = new string(m_NewLobbyName.text.Where(c => char.IsLetterOrDigit(c)).ToArray());
            if (newLobbyname.Length > 0)
            {
                _selectedLobby = newLobbyname;

                MyNetworkManager.Instance.InitLobby(newLobbyname);
                MyNetworkManager.Instance.InitConnection(MyNetworkManager.eTypeConnect.Host);
                SwitchToLobbyPanel();
            }
        }

        public void JoinLobby()
        {
            if (_selectedLobby != null)
            {
                //TODO: Only switch when connection is on
                //var ret = MyNetworkManager.Instance.InitConnection(MyNetworkManager.eTypeConnect.Join,)
                SwitchToLobbyPanel();
            }
        }

        private void SwitchToLobbyPanel()
        {
            m_LobbyPanel.gameObject.SetActive(true);
            var lobby = m_LobbyPanel.GetComponent<LobbyCanva>();
            lobby.SetLobbyName(_selectedLobby);

            gameObject.SetActive(false);
        }

        private void GetNETLobbies()
        {
            MyNetworkManager.Instance.SearchForLobbies();
        }

        public void RefreshList()
        {
            EmptyListLobbies();
            GetNETLobbies();
        }

        private void EmptyListLobbies()
        {
            _listLobbies.ForEach(o => Destroy(o.gameObject));
            _listLobbies = new();
        }

        private void CreateLobbyItemInList()
        {
            if (MyNetworkManager.Instance.Client.ListLobbyData.Count > 0)
            {
                foreach (var item in MyNetworkManager.Instance.Client.ListLobbyData)
                {
                    var go = Instantiate(m_LobbyItemsToInstantiate, m_ContentScrollView.transform);
                    LobbyListItem lobbyItem = go.GetComponent<LobbyListItem>();
                    lobbyItem.TMPName.text = item.Value.Name;
                    lobbyItem.ID = item.Value.ID;

                    _listLobbies.Add(lobbyItem);
                }
            }
        }

        #region DEBUG ONLY
        public void SendPing()
        {
            MyNetworkManager.Instance.SendPing();
        }
        #endregion
    }
}