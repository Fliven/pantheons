using Assets.Scripts.Manager;
using System.Linq;
using TMPro;
using UnityEngine;
using UnityEngine.UI;

namespace Assets.Scripts.UI
{
    public class MainMenuCanvas : MonoBehaviour
    {
        [Header("References")]
        public TMP_Text TMPPseudo;
        public GameObject m_ListLobbiesPanel;

        public string PlayerPseudo { get; set; }

        public void GotoListlobby()
        {
            PlayerPseudo = new string(TMPPseudo.text.Where(c => char.IsLetterOrDigit(c)).ToArray());

            if(PlayerPseudo.Length > 0)
            {
                m_ListLobbiesPanel.SetActive(true);
                gameObject.SetActive(false);
            }
        }
    }
}
