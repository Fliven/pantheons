﻿using System.Collections;
using UnityEngine;
using TMPro;

namespace Assets.Scripts.UI
{
    public class LobbyListItem : MonoBehaviour
    {
        [Header("References")]
        public TMP_Text TMPName;

        public uint ID { get; set; }

    }
}