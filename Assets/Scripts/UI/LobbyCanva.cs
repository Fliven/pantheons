﻿using System.Collections;
using UnityEngine;
using TMPro;
using System.Collections.Generic;
using Assets.Scripts.Manager;

namespace Assets.Scripts.UI
{
    public class LobbyCanva : MonoBehaviour
    {
        [Header("References")]
        public GameObject m_MainMenuPanel;
        public TMP_Text m_LobbyName;
        public List<LobbyPlayerCard> m_Team1Cards;
        public List<LobbyPlayerCard> m_Team2Cards;

        public void SetLobbyName(string name)
        {
            m_LobbyName.text = name;
        }

        public void AddPlayer(uint playerID, string playerName)
        {
            var cardToFill = FindEmptyCard();

            if(cardToFill != null)
            {
                cardToFill.m_PlayerCardName.text = playerName;
                cardToFill.ID = playerID;
            }
            else
            {
                Debug.LogError("No player card available");
            }
        }
        
        private LobbyPlayerCard FindEmptyCard()
        {
            foreach (var card in m_Team1Cards)
            {
                if(!card.IsOwnByPlayer)
                    return card;
            }

            foreach (var card in m_Team2Cards)
            {
                if (!card.IsOwnByPlayer)
                {
                    return card;
                }
            }

            return null;
        }

        public void SetReady()
        {

        }

        public void ReturnToHomeMenu()
        {
            MyNetworkManager.Instance.ReturnMainMenu();
            m_MainMenuPanel.SetActive(true);
            gameObject.SetActive(false);
        }
    }
}