﻿using UnityEngine;
using System.Net;
using System.Net.Sockets;
using System;
using Assets.Scripts.Networking.Client;
using Assets.Scripts.Networking.Common;
using System.Linq;

namespace Assets.Scripts.Manager
{
    public class MyNetworkManager : MonoBehaviour
    {
        #region Field
        public GameObject vClientPrefab;
        public int vPort;
        #endregion

        public static MyNetworkManager Instance;
        public Action<Data> OnUpdateForAllClient;

        #region Props
        public NET_Client Client { get; private set; }

        public eTypeConnect TypeConnect { get; private set; }

        public Action OnPlayerConnected;
        public Action OnFoundLobbies;

        public string PlayerPseudo { get; private set; }
        public string LocalIpAdresse { get; set; }
        #endregion        

        public enum eTypeConnect
        {
            Host = 0,
            Join = 1
        }

        private void Awake()
        {
            if(Instance == null)
                Instance = this;
            else
                Destroy(gameObject); 

            LocalIpAdresse = GetLocalIPAddress();
        }

        public string GetLocalIPAddress()
        {
            string ipAdress = "";

            var host = Dns.GetHostEntry(Dns.GetHostName());

            if(host != null)
                ipAdress = host.AddressList.First(o => o.AddressFamily == AddressFamily.InterNetwork).ToString();

            if (string.IsNullOrEmpty(ipAdress))
                Debug.LogError("No connexion !");

            return ipAdress;
        }

        #region Server
        public void InitLobby(string lobbyName)
        {
            LobbyData lobby = new() { Name = lobbyName };
            lobby.SetRandID();
            Client.MyLobby = lobby;
        }

        public void CloseServer()
        {
            Client.DisconnectToServer();
            Destroy(Client.gameObject);
        }
        #endregion

        #region Common
        public int InitClient(string playerName)
        {
            PlayerPseudo = playerName;
            
            return CreateClient();
        }

        public void SearchForLobbies()
        {
            Client.LookingForLobbies(vPort);
        }

        public int InitConnection(eTypeConnect typeConnect, string ipAddress = "")
        {
            TypeConnect = typeConnect;

            return ConnectClient(ipAddress, typeConnect == eTypeConnect.Host);
        }

        private int CreateClient()
        {
            try
            {
                var go = Instantiate(vClientPrefab);
                Client = go.GetComponent<NET_Client>();

                Client.SelfEndPoint = new IPEndPoint(IPAddress.Parse(LocalIpAdresse), vPort);
                Client.PlayerData = new PlayerData { Name = PlayerPseudo };

                Client.StartListener(vPort);

                return 1;
            }
            catch (Exception ex)
            {
                Debug.LogError(ex);
                Client.CancelListener();
                return -1;
            }
        }

        public void ReturnMainMenu()
        {
            if(Client != null)
                DestroyClient();
        }

        private void DestroyClient()
        {
            Client.CancelListener();
            Destroy(Client.gameObject);
        }

        private int ConnectClient(string serverIP, bool isHost = false)
        {
            Client.IPServer = isHost ? LocalIpAdresse : serverIP;
            Client.IsHost = isHost;

             if(isHost)
                Client.PlayerData.SetRandID();
            else
                Client.TryConnect(serverIP, vPort);

            return 1;            
        }

        #endregion

        #region Client
        public void Kicked()
        {
            //LobbyManager.Instance.ReturnHome();
        }

        public void PlayerUpdated()
        {
            OnUpdateForAllClient.Invoke(Client.PlayerData);
        }
        #endregion

        #region TEST ONLY
        public void SendPing()
        {
            // PC
            //Client.SendPing(new IPEndPoint(IPAddress.Parse("192.168.1.75"), vPort));

            //  MOBILE
            Client.SendPing(new IPEndPoint(IPAddress.Parse("192.168.1.37"), vPort));
        }
        #endregion
    }
}
