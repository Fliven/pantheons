﻿using UnityEngine;
using System.Threading.Tasks;
using System.Threading;
using System.Net;
using System.Net.Sockets;
using System.Collections.Concurrent;
using Assets.Scripts.Networking.Common;
using System.Linq;

namespace Assets.Scripts.Networking
{

    /// <summary>
    /// This class is the Network System.
    /// </summary>
    public abstract class NET_System : MonoBehaviour
    {

        private UdpClient _udpClient;
        /// <summary>
        /// Store the listener task is very important.
        /// Without this, we can't know if there is already a loop or not.
        /// </summary>
        private Task ListenerTask = null;
        private readonly CancellationTokenSource CancelSource = new CancellationTokenSource();

        /// <summary>
        /// The queue of validated packet, filled by the listener thread.
        /// </summary>
        private readonly ConcurrentQueue<NET_ValidatedPacket> ValidatedQueue = new ConcurrentQueue<NET_ValidatedPacket>();

        /// <summary>
        /// Max number of Packet to process on the main thread per Update.
        /// We need this limitation to not block the main thread for too long in case of a lot of packet to process.
        /// </summary>
        private const int MAX_PACKET_PER_UPDATE = 128;

        protected virtual void Update()
        {
            //In the Update loop we proceed the Packets in the Queue.

            //@TODO : Ensure there is no overflow on this Queue...
            while (ValidatedQueue.Count > 0)
            {
                if (ValidatedQueue.TryDequeue(out NET_ValidatedPacket Packet))
                {
                    OnPacketReceived(Packet.Sender, Packet.Packet);
                }
            }
        }

        public void StartListener(int port)
        {
            //Sanity check, if the ListenerTask is already running : NEVER start two listener task.
            if (ListenerTask != null && !ListenerTask.IsCompleted) return;

            //We declare the client with only the port because we listen on any ip
            _udpClient = new UdpClient(port);

            ListenerTask = Task.Run(async () =>
            {
                while (true)
                {
                    CancelSource.Token.ThrowIfCancellationRequested();
                    var Result = await _udpClient.ReceiveAsync();

                    //Create a NET_Packet from the received data.
                    NET_Packet Packet = new NET_Packet(Result.Buffer);

                    if (Result.RemoteEndPoint.Address.Equals(IPAddress.Parse(GetLocalIPAddress())))
                        continue;

                    //We check if we need to drop the packet or not.
                    if (!Packet.IsValid())
                        Debug.LogWarning("Invalid Packet");

                    if (ValidatedQueue.Count > MAX_PACKET_PER_UPDATE || !Packet.IsValid()) continue;

                    //We add the valid packet to the Queue.
                    ValidatedQueue.Enqueue(new NET_ValidatedPacket(Result.RemoteEndPoint, Packet));
                }
            }
            , CancelSource.Token);
        }

        #region UTILITIES

        public void SendTo(IPEndPoint destination, NET_Packet packet)
        {
            using (var client = new UdpClient())
            {
                byte[] buffer = packet.Serialize();
                Debug.Log($"SENDING # {packet.Key} to {destination}");
                client.Send(buffer, buffer.Length, destination);
            }
        }

        private string GetLocalIPAddress()
        {
            string ipAdress = "";

            var host = Dns.GetHostEntry(Dns.GetHostName());

            if (host != null)
                ipAdress = host.AddressList.First(o => o.AddressFamily == AddressFamily.InterNetwork).ToString();

            if (string.IsNullOrEmpty(ipAdress))
                Debug.LogError("No connexion !");

            return ipAdress;
        }

        #endregion

        #region CALLBACKS

        /// <summary>
        /// Implement this in child class to have a callback when a packet is proceed.
        /// </summary>
        /// <param name="From"></param>
        /// <param name="NewPacket"></param>
        public virtual void OnPacketReceived(IPEndPoint From, NET_Packet NewPacket) { }

        #endregion

        public void CancelListener()
        {
            Debug.Log("Listener Closed");
            CancelSource.Cancel();
            if (_udpClient != null)
                _udpClient.Close();
        }

        private void OnApplicationQuit()
        {
            // Here, we cancel the ListenerTask to prevents unity from continuing to receive packets even after play is stopped.
            CancelListener();
        }
    }
}
