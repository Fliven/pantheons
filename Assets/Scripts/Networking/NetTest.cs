﻿using UnityEngine;
using Assets.Scripts.Networking;
using System.Net;
using Assets.Scripts.Networking.Common;

/// <summary>
/// Use this class juste for testing purpose.
/// Do your own implementation by inehrit from NET_System
/// Don't forget to include Networking namespace.
/// </summary>
public class NetTest : NET_System
{

    /// <summary>
    /// Singleton if needed
    /// </summary>
    public static NetTest Instance { get; private set; } = null;

    private void Awake()
    {
        Instance = Instance ?? this;
    }

    protected override void Update()
    {
        // Very important here to call the parent function.
        base.Update();

        // Just for testing purpose if we press Space, we sent a packet to ourself.
        // You can try to change the Packet key or add Data if you want.
        if(Input.GetKeyDown(KeyCode.Space))
        {
           // SendTo(new IPEndPoint(IPAddress.Parse("127.0.0.1"), 11000),NET_Packet.Create(NET_PacketKey.NET_PING));
        }
    }

    /// <summary>
    /// Override parent function for debugging purpose.
    /// </summary>
    /// <param name="From"></param>
    /// <param name="NewPacket"></param>
    public override void OnPacketReceived(IPEndPoint From, NET_Packet NewPacket) => Debug.Log(NewPacket);
    

}
