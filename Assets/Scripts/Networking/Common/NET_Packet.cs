﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using System.Net;

namespace Assets.Scripts.Networking.Common
{
    /// <summary>
    /// This container contains a valid Packet with the sender end point.
    /// </summary>
    public struct NET_ValidatedPacket
    {
        /// <summary>
        /// The Sender of this packet.
        /// </summary>
        public IPEndPoint Sender;

        /// <summary>
        /// The packet that is necessarily valid.
        /// </summary>
        public NET_Packet Packet;

        public NET_ValidatedPacket(IPEndPoint sender, NET_Packet packet)
        {
            Sender = sender;
            Packet = packet;
        }

        /// <summary>
        /// Override ToString() to allow Packet debugging with print function.
        /// </summary>
        /// <returns></returns>
        public override string ToString() => $"FROM : {Sender.Address} PACKET : {Packet}";
    }

    /// <summary>
    /// This enum is used to identify the content of the packet and therefore how it should be interpreted
    /// </summary>
    public enum NET_PacketKey : byte
    {
        NET_INVALID = 255, // An invalid packet
        NET_SEARCH = 0, // To search for a game
        NET_CONNECT = 1, // To connect
        NET_ACCEPT = 2, // To accept a  incoming connection
        NET_REJECT = 3, // To reject an incoming connection
        NET_RPC = 4, // To share a remote procedure call
        NET_REP = 5, // To replicate an object
        NET_KICK = 6, // To kick a player
        NET_MESSAGE = 7, // To send a text message
        NET_SEARCH_RESULT = 8, // To share match's name
        NET_PING = 9, // To ping other clients or server
        NET_DISCONNECT = 10, // To tell player disconnection to the server
    }

    /// <summary>
    /// This class defines what a packet is.
    /// Just use the constructor with byte array parameter to rebuild the packet.
    /// @TODO : Add a byte determining if this packet need an ACK or not.
    /// </summary>
    [Serializable]
    public class NET_Packet
    {
        /// <summary>
        /// Replace this value by what you want.
        /// It represents the way to identify incoming packets to make sure they are destined for your game.
        /// </summary>
        private const uint GAME_SIGNATURE = 0x00E0E565; //  0x00E0E565 is just an exemple.

        /// <summary>
        /// The minimal size required for this packet to be valid.
        /// See constructor for more info.
        /// </summary>
        private const int MINIMAL_SIZE = sizeof(uint) * 2 + 1;

        /// <summary>
        /// The Key of this Packet.
        /// </summary>
        public NET_PacketKey Key { get; private set; } = NET_PacketKey.NET_INVALID;

        /// <summary>
        /// The pseudo Unique Identifier of this Packet.
        /// </summary>
        public uint UID { get; private set; } = 0;

        /// <summary>
        /// A table representing the contents of the packet.
        /// This does not include the Key or the UID.
        /// </summary>
        public byte[] Data { get; private set; } = null;

        public NET_Packet() { }

        public NET_Packet(byte[] buffer)
        {
            // Sanity check, we check if the minimal size of a packet is respected.
            // In our case we know that a packet need at least a UID (uint) a GAME_SIGNATURE (uint) and a key (byte)
            // We can translate this by sizeof(UID) + sizeof(GAME_SIGNATURE) + sizeof(Key) = 9 Bytes.
            if (buffer.Length < MINIMAL_SIZE) return;

            // Here we check if the packet received is intended for your application or not.
            if (BitConverter.ToUInt32(buffer, 0) != GAME_SIGNATURE) return;

            // Here we extract the unique identifier of the packet.
            UID = BitConverter.ToUInt32(buffer, sizeof(uint));

            // Here we extract the key of the packet.
            Key = (NET_PacketKey)buffer[sizeof(uint) * 2];

            // To extract the data, we need to check if there is any data.
            if (buffer.Length > MINIMAL_SIZE)
            {
                // Allocate the array to copy data in.
                Data = new byte[buffer.Length - MINIMAL_SIZE];

                // Copy data from buffer.
                Buffer.BlockCopy(buffer, MINIMAL_SIZE, Data, 0, Data.Length);
            }
        }

        /// <summary>
        /// Serialize the NET_Packet to a array of bytes.
        /// </summary>
        /// <returns></returns>
        public byte[] Serialize()
        {
            // Allocate an array with the size of the serialized NET_Packet
            byte[] buffer = new byte[MINIMAL_SIZE + (Data != null ? Data.Length : 0)];

            // Serialize the Game signature.
            Buffer.BlockCopy(BitConverter.GetBytes(GAME_SIGNATURE), 0, buffer, 0, sizeof(uint));

            // Serialize the UID.
            Buffer.BlockCopy(BitConverter.GetBytes(UID), 0, buffer, sizeof(uint), sizeof(uint));

            // Serialize the Key
            buffer[sizeof(uint) * 2] = (byte)Key;

            // Serialize the Data
            if (Data != null)
                Buffer.BlockCopy(Data, 0, buffer, MINIMAL_SIZE, Data.Length);

            return buffer;
        }

        /// <summary>
        /// Return a created NET_Packet with the specified <paramref name="key"/>
        /// containing the specified <paramref name="data"/>
        /// </summary>
        /// <param name="key"></param>
        /// <param name="data"></param>
        /// <returns></returns>
        public static NET_Packet Create(NET_PacketKey key, byte[] data = null)
        {
            return new NET_Packet
            {
                UID = (uint)UnityEngine.Random.Range(int.MinValue, int.MaxValue),
                Key = key,
                Data = data
            };
        }

        /// <summary>
        /// Return true if the Packet is valid, otherwise false.
        /// </summary>
        /// <returns></returns>
        public bool IsValid() => Key != NET_PacketKey.NET_INVALID;

        /// <summary>
        /// Override ToString() to allow Packet debugging with print function.
        /// </summary>
        /// <returns></returns>
        public override string ToString() => $"UID : {UID} KEY : {Key} IsValid : {IsValid()} Data Lenght : {(Data != null ? Data.Length : 0)}";

    }
}