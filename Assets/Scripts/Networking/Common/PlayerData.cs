﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Runtime.Serialization.Formatters.Binary;

namespace Assets.Scripts.Networking.Common
{
    [Serializable]
    public class PlayerData : Data
    {
        public uint ID { get; set; }
        public string Name { get; set; }
        public short Team { get; set; }
        public bool IsReady { get; set; }
        public int SelectedDeckId { get; set; }

        public PlayerData()
        {
        }

        public void SetRandID()
        {
            ID = (uint)new Random().Next(0, int.MaxValue);
        }

        public PlayerData(byte[] bytes)
        {
            var playerData = FromBytes(bytes);
            this.ID = playerData.ID;
            this.Name = playerData.Name;
            this.Team = playerData.Team;
            this.IsReady = playerData.IsReady;
        }

        public void CopyFromPlayerData(PlayerData playerData)
        {
            Team = playerData.Team;
            ID = playerData.ID;
        }

        public override byte[] Serialize()
        {
            return SerializeProcess(this);
        }

        public byte[] SerializeID()
        {
            return SerializeProcess(ID);
        }

        private byte[] SerializeProcess(object data)
        {
            BinaryFormatter bf = new BinaryFormatter();

            using (MemoryStream ms = new MemoryStream())
            {
                bf.Serialize(ms, data);
                return ms.ToArray();
            }
        }

        private PlayerData FromBytes(byte[] bytes)
        {
            PlayerData data = new();
            if (bytes != null && bytes.Length > 0)
            {
                BinaryFormatter bf = new BinaryFormatter();
                using (MemoryStream ms = new MemoryStream())
                {
                    ms.Write(bytes, 0, bytes.Length);
                    ms.Seek(0, SeekOrigin.Begin);

                    data = (PlayerData)bf.Deserialize(ms);
                }
            }
            return data;
        }
    }
}
