﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Net;
using System.Runtime.Serialization.Formatters.Binary;

namespace Assets.Scripts.Networking.Common
{
    [Serializable]
    public class LobbyData : Data
    {
        public uint ID { get; set; }
        public string Name { get; set; }

        public Dictionary<IPEndPoint,PlayerData> ListPlayers { get; set; }

        [Serializable]
        struct MinimumData
        {
            public uint ID;
            public string Name;
        }
        
        public LobbyData()
        {

        }

        public LobbyData(byte[] bytes)
        {
            var data = FromBytes(bytes);
            ID = data.ID;
            Name = data.Name;
        }

        public void SetRandID()
        {
            ID = (uint)new Random().Next(0, int.MaxValue);
        }

        #region Serialization
        public override byte[] Serialize()
        {
            //return SerializeProcess(this);

            BinaryFormatter bf = new BinaryFormatter();

            using (MemoryStream ms = new MemoryStream())
            {
                bf.Serialize(ms, this);
                return ms.ToArray();
            }
        }

        //public byte[] SerializeMinimum()
        //{
        //    return SerializeProcess(new MinimumData { ID = this.ID, Name = this.Name });
        //}
        //private byte[] SerializeProcess(object data)
        //{
        //    BinaryFormatter bf = new BinaryFormatter();

        //    using (MemoryStream ms = new MemoryStream())
        //    {
        //        bf.Serialize(ms, data);
        //        return ms.ToArray();
        //    }
        //}

        private LobbyData FromBytes(byte[] bytes)
        {
            LobbyData data = new();
            if (bytes != null && bytes.Length > 0)
            {
                BinaryFormatter bf = new BinaryFormatter();
                using (MemoryStream ms = new MemoryStream())
                {
                    ms.Write(bytes, 0, bytes.Length);
                    ms.Seek(0, SeekOrigin.Begin);

                    data = (LobbyData)bf.Deserialize(ms);
                }
            }
            return data;
        }
        #endregion
    }
}
