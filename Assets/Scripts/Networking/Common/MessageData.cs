﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Runtime.Serialization.Formatters.Binary;
using System.Text;
using System.Threading.Tasks;

namespace Assets.Scripts.Networking.Common
{
    [Serializable]
    public class MessageData : Data
    {
        public Data Data { get; set; }

        public MessageData()
        {
        }

        public MessageData(byte[] bytes)
        {
            var messageData = FromBytes(bytes);
            Data = messageData.Data;
        }

        public override byte[] Serialize()
        {
            BinaryFormatter bf = new BinaryFormatter();

            using (MemoryStream ms = new MemoryStream())
            {
                bf.Serialize(ms, this);
                return ms.ToArray();
            }
        }

        private MessageData FromBytes(byte[] bytes)
        {
            MessageData data = new();
            if (bytes != null && bytes.Length > 0)
            {
                BinaryFormatter bf = new BinaryFormatter();
                using (MemoryStream ms = new MemoryStream())
                {
                    ms.Write(bytes, 0, bytes.Length);
                    ms.Seek(0, SeekOrigin.Begin);

                    data = (MessageData)bf.Deserialize(ms);
                }
            }
            return data;
        }
    }
}
