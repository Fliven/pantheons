﻿using Assets.Scripts.Manager;
using Assets.Scripts.Networking.Common;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Threading.Tasks;
using UnityEngine;

namespace Assets.Scripts.Networking.Client
{
    public class NET_Client : NET_System
    {
        public bool IsHost { get; set; }
        private bool IsConnected { get; set; }
        public PlayerData PlayerData { get; set; }

        private const float ELAPSE_TIME_PING = 0.1f; // 100 ms
        private const float ELAPSE_TIME_TIMEOUT = 1f;

        /** Client **/
        public string IPServer { get; set; }
        private IPEndPoint _serverEndPoint;
        private float _timerPing;
        private bool _waitingForServerPing;
        private float _timerTimeOut;
        public Dictionary<IPEndPoint, LobbyData> ListLobbyData { get; private set; }

        /** Server **/
        private Dictionary<IPEndPoint, PlayerData> ListPlayers { get; set; }
        private Dictionary<uint, float> _ListPlayerTimer { get; set; }
        public LobbyData MyLobby { get; set; }

        public IPEndPoint SelfEndPoint { get; set; }

        private void Start()
        {
            ListPlayers = new Dictionary<IPEndPoint, PlayerData>();
            ListLobbyData = new();
            _ListPlayerTimer = new Dictionary<uint, float>();
            MyNetworkManager.Instance.OnUpdateForAllClient += SendDataToAllClient;
        }

        protected override void Update()
        {
            // Very important here to call the parent function.
            base.Update();

            UpdateTimers();
        }

        /// <summary>
        /// Override parent function for debugging purpose.
        /// </summary>
        /// <param name="from"></param>
        /// <param name="newPacket"></param>
        public override void OnPacketReceived(IPEndPoint from, NET_Packet newPacket)
        {
            Debug.Log($"RECEIVING # {newPacket} from : {from}");
            from.Port = MyNetworkManager.Instance.vPort;

            if (IsHost)
            {
                if (newPacket.Key == NET_PacketKey.NET_CONNECT)
                {
                    PlayerConnected(from, newPacket);
                    SendLobbyUpdate();
                }
                else if (newPacket.Key == NET_PacketKey.NET_PING)
                {
                    var playerDataReceived = new PlayerData(newPacket.Data);
                    SendPing(from);
                    if(playerDataReceived != null & _ListPlayerTimer.ContainsKey(playerDataReceived.ID))
                        _ListPlayerTimer[playerDataReceived.ID] = 0f;
                }
                else if (newPacket.Key == NET_PacketKey.NET_DISCONNECT)
                {
                    PlayerDisconnected(new PlayerData(newPacket.Data));
                    SendLobbyUpdate();
                }
                else if (newPacket.Key == NET_PacketKey.NET_MESSAGE)
                {
                    MessageData messageData = new MessageData(newPacket.Data);
                    if (messageData.Data is PlayerData playerData)
                    {
                        ListPlayers[from] = playerData;
                        //LobbyManager.Instance.UpdadateLobby(ListPlayers.Select(o => o.Value).ToList());
                        SendLobbyUpdate();
                    }
                }
                else if (newPacket.Key == NET_PacketKey.NET_SEARCH)
                {
                    SendLobbyInfo(from);
                }
            }
            else
            {
                if (newPacket.Key == NET_PacketKey.NET_ACCEPT)
                {
                    var playerDataReceived = new PlayerData(newPacket.Data);
                    _serverEndPoint = new IPEndPoint(IPAddress.Parse(IPServer), MyNetworkManager.Instance.vPort);
                    IsConnected = true;
                    PlayerData.CopyFromPlayerData(playerDataReceived);
                    MyNetworkManager.Instance.OnPlayerConnected.Invoke();
                }
                else if (newPacket.Key == NET_PacketKey.NET_PING)
                {
                    SendPing(from);
                    _waitingForServerPing = false;
                    _timerTimeOut = 0f;
                }
                else if (newPacket.Key == NET_PacketKey.NET_KICK)
                {
                    Kicked();
                }
                else if (newPacket.Key == NET_PacketKey.NET_MESSAGE)
                {
                    MessageData messageData = new MessageData(newPacket.Data);
                    if (messageData.Data is LobbyData lobbyData)
                    {
                        ListPlayers = lobbyData.ListPlayers;

                    }
                    else if (messageData.Data is PlayerData playerData)
                    {
                        ListPlayers[from] = playerData;
                    }

                    //LobbyManager.Instance.UpdadateLobby(ListPlayers.Select(o => o.Value).ToList());
                }
                else if (newPacket.Key == NET_PacketKey.NET_SEARCH_RESULT)
                {
                    if (!ListLobbyData.ContainsKey(from))
                    {
                        var lobbyData = new LobbyData(newPacket.Data);
                        ListLobbyData.Add(from, lobbyData);
                    }
                    MyNetworkManager.Instance.OnFoundLobbies?.Invoke();
                }
            }
        }

        #region HOST
        private void PlayerConnected(IPEndPoint from, NET_Packet packet)
        {
            PlayerData playerData = new PlayerData(packet.Data);
            if (!ListPlayers.ContainsKey(from))
            {
                if (ListPlayers.Count < 6)
                {
                    playerData.SetRandID();
                    //LobbyManager.Instance.Playerjoined(ref playerData);
                    ListPlayers.Add(from, playerData);
                    _ListPlayerTimer.Add(playerData.ID, 0f);

                    SendAccept(from, playerData);
                }
                else
                {
                    SendReject(from);
                }
            }
        }

        private void PlayerDisconnected(PlayerData playerData)
        {
            if (ListPlayers.ContainsValue(playerData))
            {
                //LobbyManager.Instance.PlayerLeave(playerData);
                _ListPlayerTimer.Remove(playerData.ID);
                ListPlayers.Remove(ListPlayers.First(o => o.Value == playerData).Key);
            }
        }

        private void SendLobbyUpdate()
        {
            Dictionary<IPEndPoint, PlayerData> listCopy = new Dictionary<IPEndPoint, PlayerData>(ListPlayers);
            listCopy.Add(SelfEndPoint, PlayerData);
            MyNetworkManager.Instance.OnUpdateForAllClient.Invoke(new LobbyData { ListPlayers = listCopy });
        }

        private void SendLobbyInfo(IPEndPoint from)
        {
            SendTo(from, NET_Packet.Create(NET_PacketKey.NET_SEARCH_RESULT, MyLobby.Serialize()));
        }

        private void SendAccept(IPEndPoint from, PlayerData playerData)
        {
            SendTo(from, NET_Packet.Create(NET_PacketKey.NET_ACCEPT, playerData.Serialize()));
        }

        private void SendReject(IPEndPoint from)
        {
            SendTo(from, NET_Packet.Create(NET_PacketKey.NET_REJECT));
        }

        private void SendDataToAllClient(Data data)
        {
            var bytes = DataToBytes(new MessageData { Data = data });
            if (bytes is not null)
            {
                foreach (var client in ListPlayers)
                {
                    if (client.Key != SelfEndPoint)
                        SendTo(client.Key, NET_Packet.Create(NET_PacketKey.NET_MESSAGE, bytes));
                }
            }
            else
            {
                Debug.LogError("Data null when sending datas to all clients !");
            }
        }

        private byte[] DataToBytes(Data data)
        {
            if (data is LobbyData lobbyData)
            {
                return lobbyData.Serialize();
            }
            else if (data is PlayerData playerData)
            {
                return playerData.Serialize();
            }
            else if (data is MessageData messageData)
            {
                return messageData.Serialize();
            }

            return null;
        }
        #endregion

        #region CLIENT
        public void TryConnect(string ipaddress, int port)
        {
            SendTo(new IPEndPoint(IPAddress.Parse(ipaddress), port), NET_Packet.Create(NET_PacketKey.NET_CONNECT, PlayerData.Serialize()));
        }

        private void Kicked()
        {
            MyNetworkManager.Instance.Kicked();
        }
        #endregion

        #region COMMON
        private void UpdateTimers()
        {
            if (!IsHost && IsConnected)
            {
                if (_waitingForServerPing)
                {
                    _timerTimeOut += Time.deltaTime;
                    if (_timerTimeOut >= ELAPSE_TIME_TIMEOUT)
                    {
                        Debug.LogError("Server connection Time Out");
                        Kicked();
                    }
                }

                _timerPing += Time.deltaTime;

                if (_timerPing >= ELAPSE_TIME_PING)
                {
                    SendPing(_serverEndPoint);
                    _timerPing = 0f;
                    _waitingForServerPing = true;
                }
            }
            else if (IsHost)
            {
                List<KeyValuePair<IPEndPoint, PlayerData>> playerToRemove = new List<KeyValuePair<IPEndPoint, PlayerData>>();

                foreach (var player in ListPlayers)
                {
                    if (_ListPlayerTimer.ContainsKey(player.Value.ID))
                    {
                        _ListPlayerTimer[player.Value.ID] += Time.deltaTime;

                        if (_ListPlayerTimer[player.Value.ID] > ELAPSE_TIME_TIMEOUT)
                        {
                            Debug.LogWarning($"Player {player.Value.ID} time : {_ListPlayerTimer[player.Value.ID]} > {ELAPSE_TIME_TIMEOUT}");
                            playerToRemove.Add(player);
                        }
                    }
                }

                foreach (var player in playerToRemove)
                {
                    PlayerDisconnected(player.Value);
                    SendKick(player.Key);
                    Debug.LogWarning($"Player {player.Value.Name} ({player.Value.ID}) disconnected due to a timeout");
                }
            }
        }

        public void DisconnectToServer()
        {
            IsConnected = false;
            if (!IsHost)
                SendDisconnect(_serverEndPoint);
            CancelListener();
        }

        public void LookingForLobbies(int port)
        {
            SendSearch(port);
        }

        public void SendPing(IPEndPoint endPoint)
        {
            SendTo(endPoint, NET_Packet.Create(NET_PacketKey.NET_PING, PlayerData.Serialize()));
        }

        private void SendDisconnect(IPEndPoint endPoint)
        {
            SendTo(endPoint, NET_Packet.Create(NET_PacketKey.NET_DISCONNECT, PlayerData.Serialize()));
        }

        private void SendKick(IPEndPoint endPoint)
        {
            SendTo(endPoint, NET_Packet.Create(NET_PacketKey.NET_KICK));
        }

        private void SendSearch(int port)
        {
            SendTo(new IPEndPoint(IPAddress.Broadcast, port),
                NET_Packet.Create(NET_PacketKey.NET_SEARCH));
        }
        #endregion
    }
}
